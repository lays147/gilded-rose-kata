setup/dev:
	poetry install

setup/prd:
	poetry install --no-dev

test/run:
	poetry run pytest

format/run:
	poetry run black .
	poetry run isort .
