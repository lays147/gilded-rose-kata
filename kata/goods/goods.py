from enum import Enum


class Goods(Enum):
    CONJURED = "Conjured Mana Cake"
    SULFURAS = "Sulfuras, Hand of Ragnaros"
    AGED_BRIE = "Aged Brie"
    BACKSTAGE = "Backstage passes to a TAFKAL80ETC concert"
